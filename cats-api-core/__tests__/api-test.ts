import Client from '../../dev/http-client';
import type { CatMinInfo, CatsList } from '../../dev/types';

const cats: CatMinInfo[] = [{ name: 'Попыткавторая', description: '', gender: 'male' }];

let catId: number;

const fakeId = 'fakeId';

const HttpClient = Client.getInstance();

describe('Домашнее задание по автоматизации, первая практика', () => {
  beforeAll(async () => {
    try {
      const addCatResponse = await HttpClient.post('core/cats/add', {
        responseType: 'json',
        json: { cats },
      });
      const addedCat = (addCatResponse.body as CatsList).cats[0];
      if (addedCat && addedCat.id) {
        catId = addedCat.id;
      } else {
        throw new Error('Не удалось получить id тестового котика!');
      }
    } catch (error) {
      throw new Error('Не удалось создать котика для автотестов!');
    }
  });

  afterAll(async () => {
    try {
      await HttpClient.delete(`core/cats/${catId}/remove`, {
        responseType: 'json',
      });
    } catch (error) {
      throw new Error('Не удалось удалить котика после завершения тестов!');
    }
  });

  it('Тест на поиск существующего котика по id', async () => {
    try {
      const response = await HttpClient.get(`core/cats/get-by-id?id=${catId}`, {
        responseType: 'json',
      });
    expect(response.statusCode).toEqual(200);

    expect((response.body as any).cat).toEqual(expect.objectContaining({
      id: catId,
      name: cats[0].name,
      description: cats[0].description,
      gender: cats[0].gender,
    }));

    } catch (error) {
      throw new Error(`Не удалось найти котика по id: ${catId}`);
    }
  });

  it('Тест на поиск котика по некорректному id', async () => {
    await expect(HttpClient.get(`core/cats/get-by-id?id=${fakeId}`, {
      responseType: 'json',
    })).rejects.toThrowError('Response code 400 (Bad Request)');
  });

  it('Тест на добавление описания котику', async () => {
    
    const CatDescription = 'Новое описание для котика';
    const response  = await HttpClient.post('core/cats/save-description', {
      responseType: 'json',
      json: {
          catId: catId,
          catDescription: CatDescription
      }
    });

    expect(response.statusCode).toEqual(200);
      
    expect(response.body).toEqual(expect.objectContaining({
      id: catId,
      name: cats[0].name,
      description: CatDescription,
      gender: cats[0].gender,
    }));
  });

  it('Тест на передачу неверного формата ID при сохранении описания котика', async () => {
    const CatDescription = 'Новое описание для котика';
    
    await expect(HttpClient.post('core/cats/save-description', {
      responseType: 'json',
      json: {
        catId: fakeId,
        catDescription: CatDescription
      }
    })).rejects.toThrowError('Response code 400 (Bad Request)');
  });
  
  it('Тест на получение списка котов сгруппированных по группам', async () => {

      // Опциональные параметры запроса
      const limit = 10;
      const order = 'asc';
      const gender = 'male';
  
      // Формирование строки запроса с опциональными параметрами
      const queryParams = `?limit=${limit}&order=${order}&gender=${gender}`;
  
      const response = await HttpClient.get(`core/cats/allByLetter${queryParams}`, {
        responseType: 'json',
      });
  
      expect(response.statusCode).toEqual(200);

      expect(response.body).toEqual({
        count_all: expect.any(Number),
        count_output: expect.any(Number),
        groups: expect.arrayContaining([
          expect.objectContaining({
            title: expect.any(String),
            cats: expect.arrayContaining([
              expect.objectContaining({
                id: expect.any(Number),
                name: expect.any(String),
                description: expect.any(String),
                tags: null,
                gender: expect.any(String),
                likes: expect.any(Number),
                dislikes: expect.any(Number),
                count_by_letter: expect.any(String)
              })
            ]),
            count_in_group: expect.any(Number),
            count_by_letter: expect.any(Number)
          })
        ])
      });
      
    });

});
